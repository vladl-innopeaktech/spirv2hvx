#pragma once
#include "spirv.hpp"
#include <variant>
#include <vector>
#include <map>

namespace spir {

using Id = uint32_t;

// parsed instructions
struct OpName {
    Id target;
    std::string name;
};
struct OpEntryPoint {
    Id entry_point;
    std::string name;
    std::vector<Id> interface;
};
struct OpMemberName {
    Id type;
    std::size_t member;
    std::string name;
};
struct OpTypeVoid {
    Id result;
};
struct OpTypeFunction {
    Id result;
    Id return_type;
    std::vector<Id> parameters;
};
struct OpTypeFloat {
    Id result;
    std::size_t width;
};
struct OpTypeInt {
    Id result;
    std::size_t width;
    bool signedness;
};
struct OpTypeBool {
    Id result;
};
struct OpTypeStruct {
    Id result;
    std::vector<Id> member_types;
};
struct OpTypeVector {
    Id result;
    Id component_type;
    std::size_t component_count;
};
struct OpTypePointer {
    Id result;
    spv::StorageClass storage_class;
    Id type;
};
struct OpVariable {
    Id result_type;
    Id result;
    spv::StorageClass storage_class;
    std::optional<Id> initializer;
};
struct OpConstant {
    Id result_type;
    Id result;
    std::aligned_storage<4> value;
};
struct OpConstantComposite {
    Id result_type;
    Id result;
    std::vector<Id> constituents;
};
struct OpLabel {
    Id result;
};
struct OpLoad {
    Id result_type;
    Id result;
    Id pointer;
};
struct OpStore {
    Id pointer;
    Id object;
};
struct OpFAdd {
    Id result_type;
    Id result;
    std::array<Id, 2> operand;
};
struct OpFSub {
    Id result_type;
    Id result;
    std::array<Id, 2> operand;
};
struct OpFDiv {
    Id result_type;
    Id result;
    std::array<Id, 2> operand;
};
struct OpIMul {
    Id result_type;
    Id result;
    std::array<Id, 2> operand;
};
struct OpISub {
    Id result_type;
    Id result;
    std::array<Id, 2> operand;
};
struct OpIEqual {
    Id result_type;
    Id result;
    std::array<Id, 2> operand;
};
struct OpFOrdGreaterThan {
    Id result_type;
    Id result;
    std::array<Id, 2> operand;
};
struct OpConvertFToS {
    Id result_type;
    Id result;
    Id float_value;
};
struct OpConvertSToF {
    Id result_type;
    Id result;
    Id signed_value;
};
struct OpExtInst {
    Id result_type;
    Id result;
    Id set;
    std::size_t instruction;
    std::vector<Id> operand;
};
struct OpAccessChain {
    Id result_type;
    Id result;
    Id base;
    std::vector<Id> indices;
};
struct OpCompositeExtract {
    Id result_type;
    Id result;
    Id composite;
    std::vector<std::size_t> indices;
};
struct OpCompositeConstruct {
    Id result_type;
    Id result;
    std::vector<Id> constituents;
};
struct OpSelectionMerge {
    Id merge_block;
};
struct OpBranchConditional {
    Id condition;
    Id true_label;
    Id false_label;
};
struct OpBranch {
    Id target_label;
};
struct OpFunction {
    Id result_type;
    Id result;
    Id function_type;
};
struct OpReturn {
};
struct OpPhi {
    Id result_type;
    Id result;
    std::vector<std::pair<Id, Id>> variable_parent;
};
struct OpFunctionEnd {
};

using Instruction = std::variant<
    OpName 
,   OpEntryPoint 
,   OpMemberName 
,   OpTypeVoid 
,   OpTypeFunction 
,   OpTypeFloat 
,   OpTypeInt 
,   OpTypeBool 
,   OpTypeStruct 
,   OpTypeVector 
,   OpTypePointer 
,   OpVariable 
,   OpConstant 
,   OpConstantComposite 
,   OpLabel 
,   OpLoad 
,   OpStore 
,   OpFAdd 
,   OpFSub 
,   OpFDiv 
,   OpIMul 
,   OpISub 
,   OpIEqual 
,   OpFOrdGreaterThan 
,   OpConvertFToS 
,   OpConvertSToF 
,   OpExtInst 
,   OpAccessChain 
,   OpCompositeExtract 
,   OpCompositeConstruct 
,   OpSelectionMerge 
,   OpBranchConditional 
,   OpBranch 
,   OpPhi
,   OpFunction 
,   OpReturn 
,   OpFunctionEnd 
>;

// instructions
struct RawInstruction {
    spv::Op op;
    std::vector<uint32_t> params;
};
inline auto instructions_from(std::vector<uint32_t> spirv) -> std::vector<RawInstruction> {
    using namespace spv;

    auto swap_endian = [](uint32_t v) -> uint32_t {
        return ((v >> 24) & 0x000000ffu) | ((v >> 8) & 0x0000ff00u) | ((v << 8) & 0x00ff0000u) | ((v << 24) & 0xff000000u);
    };
    auto is_valid_spirv_version = [](uint32_t version) -> bool {
        switch (version)
        {
        // Allow v99 since it tends to just work.
        case 99:
        case 0x10000: // SPIR-V 1.0
        case 0x10100: // SPIR-V 1.1
        case 0x10200: // SPIR-V 1.2
        case 0x10300: // SPIR-V 1.3
        case 0x10400: // SPIR-V 1.4
        case 0x10500: // SPIR-V 1.5
            return true;

        default:
            return false;
        }
    };

    auto len = spirv.size();
    if (len < 5) {
        throw std::runtime_error("SPIRV file too small.");
    }

    auto s = spirv.data();

    // Endian-swap if we need to.
    if (s[0] == swap_endian(MagicNumber)) {
        transform(begin(spirv), end(spirv), begin(spirv), [&](uint32_t c) { return swap_endian(c); });
    }

    if (s[0] != MagicNumber || !is_valid_spirv_version(s[1])) {
        throw std::runtime_error("Invalid SPIRV format.");
    }

    uint32_t bound = s[3];

    const uint32_t maximumNumberOfIDs = 0x3fffff;
    if (bound > maximumNumberOfIDs) {
        throw std::runtime_error("ID bound exceeds limit of 0x3fffff.\n");
    }

    uint32_t offset = 5;

    std::vector<RawInstruction> instructions;

    while (offset < len) {
        auto instr_op = spirv[offset] & 0xffff;
        auto instr_count = (spirv[offset] >> 16) & 0xffff;

        if (instr_count == 0)
            throw std::runtime_error("SPIR-V instructions cannot consume 0 words. Invalid SPIR-V file.");

        auto instr_offset = offset + 1;
        auto instr_length = instr_count - 1;

        offset += instr_count;

        if (offset > spirv.size())
            throw std::runtime_error("SPIR-V instruction goes out of bounds.");

        auto params_begin = &spirv[instr_offset];
        auto params_end = &spirv[instr_offset + instr_length];

        instructions.emplace_back(RawInstruction{static_cast<spv::Op>(instr_op), {params_begin, params_end}});
    }

    return instructions;
}
inline auto to_structured_instruction(const RawInstruction& raw) -> std::optional<Instruction> {
    auto extract_string = [](const uint32_t* begin, const uint32_t* end) -> std::string {
        std::string ret;
        for (auto it = begin; it < end; ++it)
        {
            uint32_t w = *it;

            for (uint32_t j = 0; j < 4; j++, w >>= 8)
            {
                char c = w & 0xff;
                if (c == '\0')
                    return ret;
                ret += c;
            }
        }

        throw std::runtime_error("String was not terminated before EOF");
    };

    switch(raw.op) {
    case spv::OpName:
        return OpName {
            .target = raw.params[0]
        ,   .name = extract_string(&raw.params[1], &*raw.params.end())
        };
    case spv::OpEntryPoint:
        return OpEntryPoint {
            .entry_point = raw.params[1]
        ,   .name = extract_string(&raw.params[2], &*raw.params.end())
        ,   .interface = {raw.params.begin()+3, raw.params.end()}
        };
    case spv::OpMemberName:
        return OpMemberName {
            .type = raw.params[0]
        ,   .member = raw.params[1]
        ,   .name = extract_string(&raw.params[2], &*raw.params.end())
        };
    case spv::OpTypeVoid:
        return OpTypeVoid {
            .result = raw.params[0]
        };
    case spv::OpTypeFunction:
        return OpTypeFunction {
            .result = raw.params[0]
        ,   .return_type = raw.params[1]
        ,   .parameters = {&raw.params[2], &*raw.params.end()}
        };
    case spv::OpTypeFloat:
        return OpTypeFloat {
            .result = raw.params[0]
        ,   .width = raw.params[1]
        };
    case spv::OpTypeInt:
        return OpTypeInt {
            .result = raw.params[0]
        ,   .width = raw.params[1]
        ,   .signedness = bool(raw.params[2])
        };
    case spv::OpTypeBool:
        return OpTypeBool {
            .result = raw.params[0]
        };
    case spv::OpTypeStruct:
        return OpTypeStruct {
            .result = raw.params[0]
        ,   .member_types = {&raw.params[1], &*raw.params.end()}
        };
    case spv::OpTypeVector:
        return OpTypeVector {
            .result = raw.params[0]
        ,   .component_type = raw.params[1]
        ,   .component_count = raw.params[2]
        };
    case spv::OpTypePointer:
        return OpTypePointer {
            .result = raw.params[0]
        ,   .storage_class = static_cast<spv::StorageClass>(raw.params[1])
        ,   .type = raw.params[2]
        };
    case spv::OpVariable:
        return OpVariable {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .storage_class = static_cast<spv::StorageClass>(raw.params[2])
        ,   .initializer = (raw.params.size() < 4? std::nullopt : std::optional{raw.params[3]})
        };
    case spv::OpConstant:
        return OpConstant {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .value = *reinterpret_cast<const std::aligned_storage<4>*>(&raw.params[2])
        };
    case spv::OpConstantComposite:
        return OpConstantComposite {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .constituents = {&raw.params[2], &*raw.params.end()}
        };
    case spv::OpLabel:
        return OpLabel {
            .result = raw.params[0]
        };
    case spv::OpLoad:
        return OpLoad {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .pointer = raw.params[2]
        };
    case spv::OpStore:
        return OpStore {
            .pointer = raw.params[0]
        ,   .object = raw.params[1]
        };
    case spv::OpFAdd:
        return OpFAdd {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .operand = {raw.params[2], raw.params[3]}
        };
    case spv::OpFSub:
        return OpFSub {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .operand = {raw.params[2], raw.params[3]}
        };
    case spv::OpFDiv:
        return OpFDiv {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .operand = {raw.params[2], raw.params[3]}
        };
    case spv::OpIMul:
        return OpIMul {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .operand = {raw.params[2], raw.params[3]}
        };
    case spv::OpISub:
        return OpISub {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .operand = {raw.params[2], raw.params[3]}
        };
    case spv::OpIEqual:
        return OpIEqual {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .operand = {raw.params[2], raw.params[3]}
        };
    case spv::OpFOrdGreaterThan:
        return OpFOrdGreaterThan {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .operand = {raw.params[2], raw.params[3]}
        };
    case spv::OpConvertFToS:
        return OpConvertFToS {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .float_value = raw.params[2]
        };
    case spv::OpConvertSToF:
        return OpConvertSToF {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .signed_value = raw.params[2]
        };
    case spv::OpExtInst:
        return OpExtInst {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .set = raw.params[2]
        ,   .instruction = raw.params[3]
        ,   .operand = {&raw.params[4], &*raw.params.end()}
        };
    case spv::OpAccessChain:
        return OpAccessChain {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .base = raw.params[2]
        ,   .indices = {&raw.params[3], &*raw.params.end()}
        };
    case spv::OpCompositeExtract:
        return OpCompositeExtract {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .composite = raw.params[2]
        ,   .indices = {&raw.params[3], &*raw.params.end()}
        };
    case spv::OpCompositeConstruct:
        return OpCompositeConstruct {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .constituents = {&raw.params[2], &*raw.params.end()}
        };
    case spv::OpSelectionMerge:
        return OpSelectionMerge {
            .merge_block = raw.params[0]
        };
    case spv::OpBranchConditional:
        return OpBranchConditional {
            .condition = raw.params[0]
        ,   .true_label = raw.params[1]
        ,   .false_label = raw.params[1]
        };
    case spv::OpBranch:
        return OpBranch {
            .target_label = raw.params[0]
        };
    case spv::OpFunction:
        return OpFunction {
            .result_type = raw.params[0]
        ,   .result = raw.params[1]
        ,   .function_type = raw.params[2]
        };
    case spv::OpReturn:
        return OpReturn {};
    case spv::OpFunctionEnd:
        return OpFunctionEnd {};
    default:
        return std::nullopt;
    }
};
}
