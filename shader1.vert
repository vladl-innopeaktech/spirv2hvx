#version 320 es
layout (location = 0) in vec3 gPos;
layout (location = 1) in vec2 gTexCoords;
layout (location = 2) out vec2 TexCoords;
void main()
{
    TexCoords = gTexCoords;
    vec3 v1 = gPos;
    vec3 v2 = v1 + vec3(2.00001, 2.00001, 2.00001);
    vec3 vpow = vec3(1.0001,1.0001,1.0001);
    v2 = pow(v2, vpow);
    vec3 vweight = vec3(0.7, 0.7, 0.7);
    v1 = mix(v1, v2, vweight);
    v1 -= vec3(1.4, 1.4, 1.4);
    int y_num = int(round((v1.y + 1.0f) / 0.04f));
    int modi = y_num - 2 * int(floor(float(y_num) / 2.0f));
    if(modi == 1 && v1.x > -1.0f) {
        v1.x = v1.x - 0.08f;
    }
    gl_Position = vec4(v1, 1.0f);
}
