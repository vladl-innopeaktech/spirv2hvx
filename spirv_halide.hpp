/*
 * Copyright 2015-2021 Arm Limited
 * SPDX-License-Identifier: Apache-2.0 OR MIT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * At your option, you may choose to accept this material under either:
 *  1. The Apache License, Version 2.0, found at <http://www.apache.org/licenses/LICENSE-2.0>, or
 *  2. The MIT License, found at <http://opensource.org/licenses/MIT>.
 */

#ifndef SPIRV_CROSS_CPP_HPP
#define SPIRV_CROSS_CPP_HPP

#include "spirv_glsl.hpp"
#include "spirv_utils.hpp"
#include "spirv_ir.hpp"
#include <utility>
#include <set>
#include <sstream>
#include <iostream>
#include <variant>

template<class... Fs>
struct match : Fs... {
    using Fs::operator()...;
};
template<class... Fs>
match(Fs...) -> match<Fs...>;

namespace SPIRV_CROSS_NAMESPACE
{
struct Program : std::enable_shared_from_this<Program> {
    struct Block;

    struct CodePoint : std::enable_shared_from_this<CodePoint> {
        using Ref = std::shared_ptr<CodePoint>;
        using Context = std::weak_ptr<Block>;

        CodePoint(const Context ctx, spv::Op op)
            : _op(op), _context(ctx)
            {
                if(_context.expired()) {
                    throw std::runtime_error("bad context");
                }
            }

        auto op() const -> spv::Op {
            return _op;
        }

        auto n_args() const -> size_t {
            return _args.size();
        }
        auto arg(size_t i) -> const Ref& {
            return _args.at(i);
        }
        auto arg(size_t i) const -> const Ref& {
            return _args.at(i);
        }
        void add_arg(const Ref& p) {
            _args.emplace_back(p);
        }
        void add_args(const std::vector<Ref>& ps) {
            for(auto& p : ps) {
                add_arg(p);
            }
        }

        auto type() const -> std::optional<SPIRType> {
            return _type;
        }
        void type(const SPIRType& type) {
            _type = type;
        }

        auto label() const -> std::string {
            return _label;
        }
        void label(const std::string& label) {
            _label = label;
        }

        auto id() const -> std::optional<uint32_t> {
            return _id;
        }
        void id(uint32_t id) {
            _id = id;
            if(auto ctx = parent_block()) {
                std::cout << " setting id " << std::endl;
                ctx->declare(shared_from_this());
            }
        }

        auto parent_block() const -> std::shared_ptr<Block> {
            return _context.lock();
        }
        
    private:

        spv::Op _op;
        std::vector<Ref> _args;
        std::optional<SPIRType> _type;
        std::optional<uint32_t> _id;
        std::string _label;
        Context _context;
    };
    struct Block : std::enable_shared_from_this<Block> {
        using Ref = std::shared_ptr<Block>;
        using Context = std::weak_ptr<Program>;

        Block(const Context ctx)
            : _context(ctx)
        {}

        void with_instructions(std::function<void(CodePoint::Ref)> f) {
            for(auto& i : _instructions){
                f(i);
            }
        }
        void with_instructions(std::function<void(CodePoint::Ref)> f) const {
            for(auto& i : _instructions){
                f(i);
            }
        }

        auto append_instruction(spv::Op op) -> CodePoint::Ref {
            _instructions.emplace_back(new CodePoint{ weak_from_this(), op });
            return _instructions.back();
        }

        void declare(CodePoint::Ref p) {
            _named_points.emplace(*p->id(), p);
            auto& ir = _context.lock()->ir();
        }

        void with_names(std::function<void(const CodePoint::Ref&)> f) const {
            for(auto&[_,p] : _named_points) {
                f(p);
            }
        }

        auto find(uint32_t name) -> std::optional<CodePoint::Ref> {
            if(auto it = _named_points.find(name);
                    it != _named_points.end()) {
                return it->second;
            }
            else {
                std::optional<CodePoint::Ref> ret;
                _context.lock()->with_blocks([&](auto block) {
                    if(ret == std::nullopt) {
                        if(auto it = block->_named_points.find(name);
                                it != block->_named_points.end()) {
                            ret = it->second;
                        }
                    }
                });
                return ret;
            }
        }

        auto create_stub(uint32_t name) -> CodePoint::Ref {
            auto stub = std::make_shared<CodePoint>(weak_from_this(), spv::OpUndef);
            stub->id(name);
            return stub;
        }

        auto find_or_create_stub(uint32_t name) -> CodePoint::Ref {
            if(auto p = find(name)) {
                return *p;
            }
            else {
                return create_stub(name);
            }
        }

    private:

        std::vector<CodePoint::Ref> _instructions;
        std::map<uint32_t, CodePoint::Ref> _named_points;
        Context _context;
    };

    auto append_block() -> Block::Ref {
        _blocks.emplace_back(new Block{ weak_from_this() });
        return _blocks.back();
    }

    void with_blocks(std::function<void(Block::Ref)> f) {
        for(auto& b : _blocks) {
            f(b);
        }
    }

    auto ir() const -> const ParsedIR& {
        return _ir;
    }

    Program(const ParsedIR& ir)
        : _ir(ir) {}

private:

    const ParsedIR& _ir;
    std::vector<Block::Ref> _blocks;
};

class CompilerHalide : public CompilerGLSL
{
public:

    template<class X>
    auto show(const X& x) const -> std::string {
        std::stringstream ss;
        ss << parsed(ir, x);
        return ss.str();
    }

    auto show(const uint32_t& x) const -> std::string {
        return show(ID(x));
    }

    auto show(const Program::CodePoint& p) const -> std::string {
        std::stringstream ss;
        ss << p.op() << " ";
        if(p.id()) {
            ss << "[" << (*p.id()) << "] ";
            if(p.label().empty()) {
                ss << show(*p.id()) << " ";
            }
        }
        if(not p.label().empty()) {
            ss << p.label() << " ";
        }
        if(p.type()) {
            ss << "∷ " << show(*p.type()) << " ";
        }
        if(p.n_args() > 0) {
            ss << "(";
            for(size_t i = 0; i < p.n_args(); ++i) {
                ss << show(p.arg(i));
                if(i+1 < p.n_args()) {
                    ss << ", ";
                }
            }
            ss << ")";
        }

        return ss.str();
    }

    auto show(const Program::CodePoint::Ref& p) const -> std::string {
        return show(*p);
    }


    std::shared_ptr<Program> tree;
    std::shared_ptr<Program::Block> current_block;
    std::shared_ptr<Program::CodePoint> current_codepoint;

	explicit CompilerHalide(std::vector<uint32_t> spirv_)
	    : CompilerGLSL(std::move(spirv_))
        , tree(new Program(ir))
	{
	}

	CompilerHalide(const uint32_t *ir_, size_t word_count)
	    : CompilerGLSL(ir_, word_count)
        , tree(new Program(ir))
	{
	}

	explicit CompilerHalide(const ParsedIR &ir_)
	    : CompilerGLSL(ir_)
        , tree(new Program(ir))
	{
	}

	explicit CompilerHalide(ParsedIR &&ir_)
	    : CompilerGLSL(std::move(ir_))
        , tree(new Program(ir))
	{
	}

	std::string compile() override;

	// Sets a custom symbol name that can override
	// spirv_cross_get_interface.
	//
	// Useful when several shader interfaces are linked
	// statically into the same binary.
	void set_interface_name(std::string name)
	{
		interface_name = std::move(name);
	}

private:
	void emit_header() override;
	void emit_c_linkage();
	void emit_function_prototype(SPIRFunction &func, const Bitset &return_flags) override;
	void emit_function(SPIRFunction &func, const Bitset &return_flags) override;

	void emit_resources();
	void emit_buffer_block(const SPIRVariable &type) override;
	void emit_push_constant_block(const SPIRVariable &var) override;
	void emit_interface_block(const SPIRVariable &type);
	void emit_uniform(const SPIRVariable &var) override;
	void emit_shared(const SPIRVariable &var);
	void emit_block_struct(SPIRType &type);
	std::string variable_decl(const SPIRType &type, const std::string &name, uint32_t id = 0) override;
	std::string variable_decl(const SPIRVariable &variable) override;
	void emit_glsl_op(uint32_t result_type, uint32_t result_id, uint32_t op, const uint32_t *args,
	                          uint32_t count) override;
	void emit_mix_op(uint32_t result_type, uint32_t id, uint32_t left, uint32_t right, uint32_t lerp);
	void emit_instruction(const Instruction &instr) override;

	void emit_unary_func_op_cast(uint32_t result_type, uint32_t result_id, uint32_t op0, const char *op,
	                             SPIRType::BaseType input_type, SPIRType::BaseType expected_result_type) override;
	void emit_store_statement(uint32_t lhs_expression, uint32_t rhs_expression) override;

	void branch(BlockID from, uint32_t cond, BlockID true_block, BlockID false_block) override;
	void branch(BlockID from, BlockID to) override;
	void flush_phi(BlockID from, BlockID to) override;
	void emit_block_chain(SPIRBlock &block) override;

	std::string argument_decl(const SPIRFunction::Parameter &arg);

	std::string image_type_glsl(const SPIRType &type, uint32_t id) override;
	std::string to_texture_op(const Instruction &i, bool sparse, bool *forward,
	                                  SmallVector<uint32_t> &inherited_expressions) override;
	std::string type_to_glsl(const SPIRType &type, uint32_t id = 0) override;

	SmallVector<std::string> resource_registrations;
    std::set<std::string> stage_variables;
    std::set<std::tuple<std::string, std::string>> stage_buffers;
	uint32_t shared_counter = 0;

	std::string interface_name;

    std::optional<uint32_t> current_emitting_block_condition;
    struct SelectOperator {
        std::string cond, lhs, true_rhs, false_rhs;
    };
    std::optional<SelectOperator> current_select_operator;

	void flush_variable_declaration(uint32_t id) override;

	std::string access_chain(uint32_t base, const uint32_t *indices, uint32_t count, const SPIRType &target_type,
	                         AccessChainMeta *meta = nullptr, bool ptr_chain = false) override;
	std::string access_chain_internal(uint32_t base, const uint32_t *indices, uint32_t count, AccessChainFlags flags,
	                                  AccessChainMeta *meta) override;
    
	std::string build_composite_combiner(uint32_t result_type, const uint32_t *elems, uint32_t length) override;

	std::string constant_expression_vector(const SPIRConstant &c, uint32_t vector) override;

    std::string to_composite_constructor_expression(uint32_t id, bool uses_buffer_offset);
};

} // namespace SPIRV_OW

#endif
