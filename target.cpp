#include "Halide.h"
using namespace Halide;
struct Shader : Func
{
    std::map<std::string, Buffer<float>> input;
    std::map<std::string, Buffer<float>> output;

    Shader(int n_verts = 3) {
        input.emplace("gPos", Buffer<float>(Float(32), {n_verts, 4}, "gPos"));
        input.emplace("gTexCoords", Buffer<float>(Float(32), {n_verts, 2}, "gTexCoords"));
        output.emplace("TexCoords", Buffer<float>(Float(32), {n_verts, 2}, "TexCoords"));
        output.emplace("gl_Position", Buffer<float>(Float(32), {n_verts, 4}, "gl_Position"));
    }

    void compute() {
        Var i("vertexId")
            , xyz("xyzCoord")
            , xyzw("xyzwCoord")
            , uv("uvCoord")
            ;

        Func gPos("gPos");
        gPos(i, xyzw) = input.at("gPos")(i, xyzw);

        Func gTexCoords("gTexCoords");
        gTexCoords(i, uv) = input.at("gTexCoords")(i, uv);

        // ----

        Func TexCoords("TexCoords");
        Func gl_Position("gl_Position");

        // ----

        TexCoords(i, uv) = gTexCoords(i, uv);

        Func v1_0("v1₀");
        v1_0(i, xyz) = gPos(i, xyz);

        Func v2_0("v2₀");
        v2_0(i, xyz) = v1_0(i, xyz) + float(2.000010013580322265625f);

        Func vpow("vpow");
        vpow(xyz) = float(1.00010001659393310546875f);

        Func v2_1("v2₁");
        v2_1(i, xyz) = pow(v2_0(i, xyz), vpow(xyz));

        Func vweight("vweight");
        vweight(xyz) = float(0.699999988079071044921875f);

        Func v1_1("v1₁");
        v1_1(i, xyz) = lerp(v1_0(i, xyz), v2_1(i, xyz), vweight(xyz));

        Func v1_2("v1₂");
        v1_2(i, xyz) = v1_1(i, xyz) - float(1.39999997615814208984375f);

        Expr y_num("y_num"); y_num = cast<int32_t>(round((v1_2(i, 1) + 1.0f) / 0.039999999105930328369140625f));
        Expr modi("modi"); modi = y_num - (2 * cast<int32_t>(floor(cast<float>(y_num) / 2.0f)));
        Expr _69("_69"); _69 = modi == 1;
        Expr _77("_77"); _77;
        _77 = select(_69, v1_2(i, 0) > (-1.0f), _69);

        Func v1_3("v1₃");
        v1_3(i, xyz) = mux(xyz, {
                select(_77, v1_2(i, 0) - 0.07999999821186065673828125f, v1_2(i, 0))
            ,   v1_2(i, 1)
            ,   v1_2(i, 2)
            });

        gl_Position(i, xyzw) = mux(i, {
                v1_3(i, 0) 
            ,   v1_3(i, 1)  
            ,   v1_3(i, 2) 
            ,   1.0f
            });

        // ----

        gl_Position.realize(output.at("gl_Position"));
        TexCoords.realize(output.at("TexCoords"));
    }
};
