#include <exception>
#include <sstream>
#include <fstream>
#include <iostream>
#include <memory>
#include "spirv_parser.hpp"
#include "spirv_halide.hpp"
#include "spirv_utils.hpp"

auto read_spirv_file(const char *path) -> std::vector<uint32_t>
{
	FILE *file = std::fopen(path, "rb");
	if (!file)
	{
		fprintf(stderr, "Failed to open SPIR-V file: %s\n", path);
		return {};
	}

    std::fseek(file, 0, SEEK_END);
	long len = std::ftell(file) / sizeof(uint32_t);
    std::rewind(file);

    std::vector<uint32_t> spirv(len);
	if (std::fread(spirv.data(), sizeof(uint32_t), len, file) != size_t(len))
		spirv.clear();

    std::fclose(file);
	return spirv;
}

template<class X>
auto operator<<(std::ostream& out, const std::vector<X>& xs) -> std::ostream& {
    for(auto it = xs.begin(); it != xs.end(); ++it) {
        out << *it;

        auto next = it; ++next;
        if(next != xs.end()) {
            out << ", ";
        }
    }

    return out;
}

int main(int argc, char** argv) {
    if(argc != 2) {
        auto ss = std::stringstream{};
        ss << "usage: " << argv[0] << ' ' << "SPV_PATH";
        throw std::runtime_error(ss.str());
    }

    auto spirv = read_spirv_file(argv[1]);

    constexpr bool ast_based_dev_approach = false;
    if(ast_based_dev_approach) {
        auto spir_main = spir::compile(spir::instructions_from(spirv));
    }
    else {
        auto compiler = SPIRV_CROSS_NAMESPACE::Compiler{std::move(spirv)};
        auto output = compiler.compile();
        std::cout << output << std::endl;
    }

    return EXIT_SUCCESS;
}
