#pragma once
#include <sstream>
#include <string>
#include <map>
#include "spirv_common.hpp"

namespace SPIRV_CROSS_NAMESPACE {
template<class T>
auto fetch(ParsedIR& ir, uint16_t id) -> T& {
    return ir.ids[id].get<T>();
}

template<class T>
struct Parsed : std::tuple<const ParsedIR&, const T&> {
    using std::tuple<const ParsedIR&, const T&>::tuple;

    auto operator()() const -> const T& {
        return object();
    }

    template<class U>
    auto operator()(const U& x) const -> Parsed<U> {
        return Parsed<U>{ir(), x};
    }

    operator const T&() const {
        return object();
    }

    auto object() const -> const T& {
        return std::get<1>(*this);
    }

    auto ir() const -> const ParsedIR& {
        return std::get<0>(*this);
    }
};
}

namespace SPIRV_CROSS_NAMESPACE {
template<class T>
auto parsed(const ParsedIR& ir, const T& x) -> Parsed<T> {
    return {ir, x};
}


inline auto operator<<(std::ostream& out, const Parsed<ID>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<TypeID>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<SPIRVariable>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<SPIRExpression>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<SPIRConstant>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<Variant>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<Instruction>& op) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<SPIRBlock>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<BlockID>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<SPIRType>& parsed) -> std::ostream&;
inline auto operator<<(std::ostream& out, const Parsed<SPIRBlock::Phi>& parsed) -> std::ostream&;

auto operator<<(std::ostream& out, const Parsed<ID>& parsed) -> std::ostream& {
    return out << parsed(parsed.ir().ids[parsed()]);
}
auto operator<<(std::ostream& out, const Parsed<TypeID>& parsed) -> std::ostream& {
    return out << parsed(parsed.ir().ids[parsed()].get<SPIRType>());
}
auto operator<<(std::ostream& out, const Parsed<SPIRVariable>& parsed) -> std::ostream& {
    if(parsed().phi_variable) {
        out << "Φ ";
    }

    out << "∷ " << parsed(parsed().basetype);

    return out;
}
auto operator<<(std::ostream& out, const Parsed<SPIRExpression>& parsed) -> std::ostream& {
    return out << '"' << parsed().expression << '"';
}
auto operator<<(std::ostream& out, const Parsed<SPIRConstant>& parsed) -> std::ostream& {
    auto& type = parsed.ir().ids[parsed().constant_type].get<SPIRType>();
    switch(type.basetype) {
		case SPIRType::Unknown: 
            return out;
		case SPIRType::Void: 
            return out << "(void)";
		case SPIRType::Boolean:
            return out << (parsed().scalar()? "true" : "false");
		case SPIRType::SByte:
            return out << parsed().scalar_i8();
		case SPIRType::UByte:
            return out << parsed().scalar_u8();
		case SPIRType::Short:
            return out << parsed().scalar_i16();
		case SPIRType::UShort:
            return out << parsed().scalar_u16();
		case SPIRType::Int:
            return out << parsed().scalar_i32();
		case SPIRType::UInt:
            return out << parsed().scalar();
		case SPIRType::Int64:
            return out << parsed().scalar_i64();
		case SPIRType::UInt64:
            return out << parsed().scalar_u64();
		case SPIRType::AtomicCounter:
            return out << "[counter]";
		case SPIRType::Half:
            return out << parsed().scalar_f16();
		case SPIRType::Float:
            return out << parsed().scalar_f32();
		case SPIRType::Double:
            return out << parsed().scalar_f64();
		case SPIRType::Struct:
            return out << "{struct}";
		case SPIRType::Image:
            return out << "{image}";
		case SPIRType::SampledImage:
            return out << "{sampledimage}";
		case SPIRType::Sampler:
            return out << "{sampler}";
		case SPIRType::AccelerationStructure:
            return out << "{accelerationstructure}";
		case SPIRType::RayQuery:
            return out << "{rayquery}";
		default:
            return out;
    }
}
auto operator<<(std::ostream& out, const Parsed<Variant>& parsed) -> std::ostream& {
    switch(parsed().get_type()) {
        case TypeVariable:
            return out << parsed(parsed().get<SPIRVariable>());
        case TypeBlock:
            return out << parsed(parsed().get<SPIRBlock>());
        case TypeType:
            return out << parsed(parsed().get<SPIRType>());
        case TypeNone:
            return out << "❔";
        case TypeConstant:
            return out << parsed(parsed().get<SPIRConstant>());
        case TypeFunction:
            return out <<  "?[function]";
        case TypeFunctionPrototype:
            return out <<  "?[functionPrototype]";
        case TypeExtension:
            return out <<  "?[extension]";
        case TypeExpression:
            return out << parsed(parsed().get<SPIRExpression>());
        case TypeConstantOp:
            return out <<  "?[constantOp]";
        case TypeCombinedImageSampler:
            return out <<  "?[combinedImageSampler]";
        case TypeAccessChain:
            return out <<  "?[accessChain]";
        case TypeUndef:
            return out <<  "?[undef]";
        case TypeString:
            return out <<  "?[string]";
        default:
            throw 0;
    }
}
auto operator<<(std::ostream& out, const Parsed<Instruction>& parsed) -> std::ostream& {
    out << spv::Op(parsed().op);

    auto& spirv = parsed.ir().spirv;

    for(auto i = parsed().offset; i < parsed().offset + parsed().length; ++i) {
        out << " (" << parsed(parsed.ir().ids[spirv[i]]) << ")";
    }

    return out;
}
auto operator<<(std::ostream& out, const Parsed<BlockID>& parsed) -> std::ostream& {
    return out << parsed(parsed.ir().ids[parsed()].get<SPIRBlock>());
}
auto operator<<(std::ostream& out, const Parsed<SPIRBlock>& parsed) -> std::ostream& {
    out << "{\n";
    for(auto op : parsed().ops) {
        out << '\t' << parsed(op) << '\n';
    }
    out << "}";
    if(0)
    switch(parsed().terminator) {
        case SPIRBlock::Direct:
            return out;// << " next\n" << parsed(parsed().next_block);
        case SPIRBlock::Select:
            return out << "\nif (" << parsed(parsed().condition) << ")"
                        << "\nthen " << parsed(parsed().true_block)
                        << "\nelse " << parsed(parsed().false_block) << "\n";
        default:
            return out;
    }

    return out;
}
auto operator<<(std::ostream& out, const Parsed<SPIRType>& parsed) -> std::ostream& {
    static auto type_names = std::map<uint16_t, std::string>{
        { SPIRType::Unknown, "Unknown" },
        { SPIRType::Void, "Void" },
        { SPIRType::Boolean, "Boolean" },
        { SPIRType::SByte, "SByte" },
        { SPIRType::UByte, "UByte" },
        { SPIRType::Short, "Short" },
        { SPIRType::UShort, "UShort" },
        { SPIRType::Int, "Int" },
        { SPIRType::UInt, "UInt" },
        { SPIRType::Int64, "Int64" },
        { SPIRType::UInt64, "UInt64" },
        { SPIRType::AtomicCounter, "AtomicCounter" },
        { SPIRType::Half, "Half" },
        { SPIRType::Float, "Float" },
        { SPIRType::Double, "Double" },
        { SPIRType::Image, "Image" },
        { SPIRType::SampledImage, "SampledImage" },
        { SPIRType::Sampler, "Sampler" },
        { SPIRType::AccelerationStructure, "AccelerationStructure" },
        { SPIRType::RayQuery, "RayQuery" },
        { SPIRType::ControlPointArray, "ControlPointArray" },
        { SPIRType::Interpolant, "Interpolant" },
        { SPIRType::Char, "Char" },
    };
    if(parsed().basetype == SPIRType::Struct) {
        out << "{";
        for(size_t i = 0; i < parsed().member_types.size(); ++i) {
            out << parsed(parsed().member_types[i]);
            if(i < parsed().member_types.size() - 1) {
                out << ", ";
            }
        }
        out << "}";
    }
    else {
        out << type_names[parsed().basetype];
    }
    if(parsed().vecsize > 1) {
        out << "×" << parsed().vecsize;
    }
    out << (parsed().pointer? "*":"");

    return out;
}
auto operator<<(std::ostream& out, const Parsed<SPIRBlock::Phi>& parsed) -> std::ostream& {
    out << parsed(parsed().local_variable) << ", " << parsed(parsed().function_variable) << ": Φ";

    return out;
}


}

inline
auto operator<<(std::ostream& out, const spv::Op& op) -> std::ostream& {
    static auto op_names = std::map<uint16_t, std::string>{
        {0, "OpNop"},
        {1, "OpUndef"},
        {2, "OpSourceContinued"},
        {3, "OpSource"},
        {4, "OpSourceExtension"},
        {5, "OpName"},
        {6, "OpMemberName"},
        {7, "OpString"},
        {8, "OpLine"},
        {10, "OpExtension"},
        {11, "OpExtInstImport"},
        {12, "OpExtInst"},
        {14, "OpMemoryModel"},
        {15, "OpEntryPoint"},
        {16, "OpExecutionMode"},
        {17, "OpCapability"},
        {19, "OpTypeVoid"},
        {20, "OpTypeBool"},
        {21, "OpTypeInt"},
        {22, "OpTypeFloat"},
        {23, "OpTypeVector"},
        {24, "OpTypeMatrix"},
        {25, "OpTypeImage"},
        {26, "OpTypeSampler"},
        {27, "OpTypeSampledImage"},
        {28, "OpTypeArray"},
        {29, "OpTypeRuntimeArray"},
        {30, "OpTypeStruct"},
        {31, "OpTypeOpaque"},
        {32, "OpTypePointer"},
        {33, "OpTypeFunction"},
        {34, "OpTypeEvent"},
        {35, "OpTypeDeviceEvent"},
        {36, "OpTypeReserveId"},
        {37, "OpTypeQueue"},
        {38, "OpTypePipe"},
        {39, "OpTypeForwardPointer"},
        {41, "OpConstantTrue"},
        {42, "OpConstantFalse"},
        {43, "OpConstant"},
        {44, "OpConstantComposite"},
        {45, "OpConstantSampler"},
        {46, "OpConstantNull"},
        {48, "OpSpecConstantTrue"},
        {49, "OpSpecConstantFalse"},
        {50, "OpSpecConstant"},
        {51, "OpSpecConstantComposite"},
        {52, "OpSpecConstantOp"},
        {54, "OpFunction"},
        {55, "OpFunctionParameter"},
        {56, "OpFunctionEnd"},
        {57, "OpFunctionCall"},
        {59, "OpVariable"},
        {60, "OpImageTexelPointer"},
        {61, "OpLoad"},
        {62, "OpStore"},
        {63, "OpCopyMemory"},
        {64, "OpCopyMemorySized"},
        {65, "OpAccessChain"},
        {66, "OpInBoundsAccessChain"},
        {67, "OpPtrAccessChain"},
        {68, "OpArrayLength"},
        {69, "OpGenericPtrMemSemantics"},
        {70, "OpInBoundsPtrAccessChain"},
        {71, "OpDecorate"},
        {72, "OpMemberDecorate"},
        {73, "OpDecorationGroup"},
        {74, "OpGroupDecorate"},
        {75, "OpGroupMemberDecorate"},
        {77, "OpVectorExtractDynamic"},
        {78, "OpVectorInsertDynamic"},
        {79, "OpVectorShuffle"},
        {80, "OpCompositeConstruct"},
        {81, "OpCompositeExtract"},
        {82, "OpCompositeInsert"},
        {83, "OpCopyObject"},
        {84, "OpTranspose"},
        {86, "OpSampledImage"},
        {87, "OpImageSampleImplicitLod"},
        {88, "OpImageSampleExplicitLod"},
        {89, "OpImageSampleDrefImplicitLod"},
        {90, "OpImageSampleDrefExplicitLod"},
        {91, "OpImageSampleProjImplicitLod"},
        {92, "OpImageSampleProjExplicitLod"},
        {93, "OpImageSampleProjDrefImplicitLod"},
        {94, "OpImageSampleProjDrefExplicitLod"},
        {95, "OpImageFetch"},
        {96, "OpImageGather"},
        {97, "OpImageDrefGather"},
        {98, "OpImageRead"},
        {99, "OpImageWrite"},
        {100, "OpImage"},
        {101, "OpImageQueryFormat"},
        {102, "OpImageQueryOrder"},
        {103, "OpImageQuerySizeLod"},
        {104, "OpImageQuerySize"},
        {105, "OpImageQueryLod"},
        {106, "OpImageQueryLevels"},
        {107, "OpImageQuerySamples"},
        {109, "OpConvertFToU"},
        {110, "OpConvertFToS"},
        {111, "OpConvertSToF"},
        {112, "OpConvertUToF"},
        {113, "OpUConvert"},
        {114, "OpSConvert"},
        {115, "OpFConvert"},
        {116, "OpQuantizeToF16"},
        {117, "OpConvertPtrToU"},
        {118, "OpSatConvertSToU"},
        {119, "OpSatConvertUToS"},
        {120, "OpConvertUToPtr"},
        {121, "OpPtrCastToGeneric"},
        {122, "OpGenericCastToPtr"},
        {123, "OpGenericCastToPtrExplicit"},
        {124, "OpBitcast"},
        {126, "OpSNegate"},
        {127, "OpFNegate"},
        {128, "OpIAdd"},
        {129, "OpFAdd"},
        {130, "OpISub"},
        {131, "OpFSub"},
        {132, "OpIMul"},
        {133, "OpFMul"},
        {134, "OpUDiv"},
        {135, "OpSDiv"},
        {136, "OpFDiv"},
        {137, "OpUMod"},
        {138, "OpSRem"},
        {139, "OpSMod"},
        {140, "OpFRem"},
        {141, "OpFMod"},
        {142, "OpVectorTimesScalar"},
        {143, "OpMatrixTimesScalar"},
        {144, "OpVectorTimesMatrix"},
        {145, "OpMatrixTimesVector"},
        {146, "OpMatrixTimesMatrix"},
        {147, "OpOuterProduct"},
        {148, "OpDot"},
        {149, "OpIAddCarry"},
        {150, "OpISubBorrow"},
        {151, "OpUMulExtended"},
        {152, "OpSMulExtended"},
        {154, "OpAny"},
        {155, "OpAll"},
        {156, "OpIsNan"},
        {157, "OpIsInf"},
        {158, "OpIsFinite"},
        {159, "OpIsNormal"},
        {160, "OpSignBitSet"},
        {161, "OpLessOrGreater"},
        {162, "OpOrdered"},
        {163, "OpUnordered"},
        {164, "OpLogicalEqual"},
        {165, "OpLogicalNotEqual"},
        {166, "OpLogicalOr"},
        {167, "OpLogicalAnd"},
        {168, "OpLogicalNot"},
        {169, "OpSelect"},
        {170, "OpIEqual"},
        {171, "OpINotEqual"},
        {172, "OpUGreaterThan"},
        {173, "OpSGreaterThan"},
        {174, "OpUGreaterThanEqual"},
        {175, "OpSGreaterThanEqual"},
        {176, "OpULessThan"},
        {177, "OpSLessThan"},
        {178, "OpULessThanEqual"},
        {179, "OpSLessThanEqual"},
        {180, "OpFOrdEqual"},
        {181, "OpFUnordEqual"},
        {182, "OpFOrdNotEqual"},
        {183, "OpFUnordNotEqual"},
        {184, "OpFOrdLessThan"},
        {185, "OpFUnordLessThan"},
        {186, "OpFOrdGreaterThan"},
        {187, "OpFUnordGreaterThan"},
        {188, "OpFOrdLessThanEqual"},
        {189, "OpFUnordLessThanEqual"},
        {190, "OpFOrdGreaterThanEqual"},
        {191, "OpFUnordGreaterThanEqual"},
        {194, "OpShiftRightLogical"},
        {195, "OpShiftRightArithmetic"},
        {196, "OpShiftLeftLogical"},
        {197, "OpBitwiseOr"},
        {198, "OpBitwiseXor"},
        {199, "OpBitwiseAnd"},
        {200, "OpNot"},
        {201, "OpBitFieldInsert"},
        {202, "OpBitFieldSExtract"},
        {203, "OpBitFieldUExtract"},
        {204, "OpBitReverse"},
        {205, "OpBitCount"},
        {207, "OpDPdx"},
        {208, "OpDPdy"},
        {209, "OpFwidth"},
        {210, "OpDPdxFine"},
        {211, "OpDPdyFine"},
        {212, "OpFwidthFine"},
        {213, "OpDPdxCoarse"},
        {214, "OpDPdyCoarse"},
        {215, "OpFwidthCoarse"},
        {218, "OpEmitVertex"},
        {219, "OpEndPrimitive"},
        {220, "OpEmitStreamVertex"},
        {221, "OpEndStreamPrimitive"},
        {224, "OpControlBarrier"},
        {225, "OpMemoryBarrier"},
        {227, "OpAtomicLoad"},
        {228, "OpAtomicStore"},
        {229, "OpAtomicExchange"},
        {230, "OpAtomicCompareExchange"},
        {231, "OpAtomicCompareExchangeWeak"},
        {232, "OpAtomicIIncrement"},
        {233, "OpAtomicIDecrement"},
        {234, "OpAtomicIAdd"},
        {235, "OpAtomicISub"},
        {236, "OpAtomicSMin"},
        {237, "OpAtomicUMin"},
        {238, "OpAtomicSMax"},
        {239, "OpAtomicUMax"},
        {240, "OpAtomicAnd"},
        {241, "OpAtomicOr"},
        {242, "OpAtomicXor"},
        {245, "OpPhi"},
        {246, "OpLoopMerge"},
        {247, "OpSelectionMerge"},
        {248, "OpLabel"},
        {249, "OpBranch"},
        {250, "OpBranchConditional"},
        {251, "OpSwitch"},
        {252, "OpKill"},
        {253, "OpReturn"},
        {254, "OpReturnValue"},
        {255, "OpUnreachable"},
        {256, "OpLifetimeStart"},
        {257, "OpLifetimeStop"},
        {259, "OpGroupAsyncCopy"},
        {260, "OpGroupWaitEvents"},
        {261, "OpGroupAll"},
        {262, "OpGroupAny"},
        {263, "OpGroupBroadcast"},
        {264, "OpGroupIAdd"},
        {265, "OpGroupFAdd"},
        {266, "OpGroupFMin"},
        {267, "OpGroupUMin"},
        {268, "OpGroupSMin"},
        {269, "OpGroupFMax"},
        {270, "OpGroupUMax"},
        {271, "OpGroupSMax"},
        {274, "OpReadPipe"},
        {275, "OpWritePipe"},
        {276, "OpReservedReadPipe"},
        {277, "OpReservedWritePipe"},
        {278, "OpReserveReadPipePackets"},
        {279, "OpReserveWritePipePackets"},
        {280, "OpCommitReadPipe"},
        {281, "OpCommitWritePipe"},
        {282, "OpIsValidReserveId"},
        {283, "OpGetNumPipePackets"},
        {284, "OpGetMaxPipePackets"},
        {285, "OpGroupReserveReadPipePackets"},
        {286, "OpGroupReserveWritePipePackets"},
        {287, "OpGroupCommitReadPipe"},
        {288, "OpGroupCommitWritePipe"},
        {291, "OpEnqueueMarker"},
        {292, "OpEnqueueKernel"},
        {293, "OpGetKernelNDrangeSubGroupCount"},
        {294, "OpGetKernelNDrangeMaxSubGroupSize"},
        {295, "OpGetKernelWorkGroupSize"},
        {296, "OpGetKernelPreferredWorkGroupSizeMultiple"},
        {297, "OpRetainEvent"},
        {298, "OpReleaseEvent"},
        {299, "OpCreateUserEvent"},
        {300, "OpIsValidEvent"},
        {301, "OpSetUserEventStatus"},
        {302, "OpCaptureEventProfilingInfo"},
        {303, "OpGetDefaultQueue"},
        {304, "OpBuildNDRange"},
        {305, "OpImageSparseSampleImplicitLod"},
        {306, "OpImageSparseSampleExplicitLod"},
        {307, "OpImageSparseSampleDrefImplicitLod"},
        {308, "OpImageSparseSampleDrefExplicitLod"},
        {309, "OpImageSparseSampleProjImplicitLod"},
        {310, "OpImageSparseSampleProjExplicitLod"},
        {311, "OpImageSparseSampleProjDrefImplicitLod"},
        {312, "OpImageSparseSampleProjDrefExplicitLod"},
        {313, "OpImageSparseFetch"},
        {314, "OpImageSparseGather"},
        {315, "OpImageSparseDrefGather"},
        {316, "OpImageSparseTexelsResident"},
        {317, "OpNoLine"},
        {318, "OpAtomicFlagTestAndSet"},
        {319, "OpAtomicFlagClear"},
        {320, "OpImageSparseRead"},
        {321, "OpSizeOf"},
        {322, "OpTypePipeStorage"},
        {323, "OpConstantPipeStorage"},
        {324, "OpCreatePipeFromPipeStorage"},
        {325, "OpGetKernelLocalSizeForSubgroupCount"},
        {326, "OpGetKernelMaxNumSubgroups"},
        {327, "OpTypeNamedBarrier"},
        {328, "OpNamedBarrierInitialize"},
        {329, "OpMemoryNamedBarrier"},
        {330, "OpModuleProcessed"},
        {331, "OpExecutionModeId"},
        {332, "OpDecorateId"},
        {333, "OpGroupNonUniformElect"},
        {334, "OpGroupNonUniformAll"},
        {335, "OpGroupNonUniformAny"},
        {336, "OpGroupNonUniformAllEqual"},
        {337, "OpGroupNonUniformBroadcast"},
        {338, "OpGroupNonUniformBroadcastFirst"},
        {339, "OpGroupNonUniformBallot"},
        {340, "OpGroupNonUniformInverseBallot"},
        {341, "OpGroupNonUniformBallotBitExtract"},
        {342, "OpGroupNonUniformBallotBitCount"},
        {343, "OpGroupNonUniformBallotFindLSB"},
        {344, "OpGroupNonUniformBallotFindMSB"},
        {345, "OpGroupNonUniformShuffle"},
        {346, "OpGroupNonUniformShuffleXor"},
        {347, "OpGroupNonUniformShuffleUp"},
        {348, "OpGroupNonUniformShuffleDown"},
        {349, "OpGroupNonUniformIAdd"},
        {350, "OpGroupNonUniformFAdd"},
        {351, "OpGroupNonUniformIMul"},
        {352, "OpGroupNonUniformFMul"},
        {353, "OpGroupNonUniformSMin"},
        {354, "OpGroupNonUniformUMin"},
        {355, "OpGroupNonUniformFMin"},
        {356, "OpGroupNonUniformSMax"},
        {357, "OpGroupNonUniformUMax"},
        {358, "OpGroupNonUniformFMax"},
        {359, "OpGroupNonUniformBitwiseAnd"},
        {360, "OpGroupNonUniformBitwiseOr"},
        {361, "OpGroupNonUniformBitwiseXor"},
        {362, "OpGroupNonUniformLogicalAnd"},
        {363, "OpGroupNonUniformLogicalOr"},
        {364, "OpGroupNonUniformLogicalXor"},
        {365, "OpGroupNonUniformQuadBroadcast"},
        {366, "OpGroupNonUniformQuadSwap"},
        {400, "OpCopyLogical"},
        {401, "OpPtrEqual"},
        {402, "OpPtrNotEqual"},
        {403, "OpPtrDiff"},
        {4416, "OpTerminateInvocation"},
        {4421, "OpSubgroupBallotKHR"},
        {4422, "OpSubgroupFirstInvocationKHR"},
        {4428, "OpSubgroupAllKHR"},
        {4429, "OpSubgroupAnyKHR"},
        {4430, "OpSubgroupAllEqualKHR"},
        {4432, "OpSubgroupReadInvocationKHR"},
        {4445, "OpTraceRayKHR"},
        {4446, "OpExecuteCallableKHR"},
        {4447, "OpConvertUToAccelerationStructureKHR"},
        {4448, "OpIgnoreIntersectionKHR"},
        {4449, "OpTerminateRayKHR"},
        {4472, "OpTypeRayQueryKHR"},
        {4473, "OpRayQueryInitializeKHR"},
        {4474, "OpRayQueryTerminateKHR"},
        {4475, "OpRayQueryGenerateIntersectionKHR"},
        {4476, "OpRayQueryConfirmIntersectionKHR"},
        {4477, "OpRayQueryProceedKHR"},
        {4479, "OpRayQueryGetIntersectionTypeKHR"},
        {5000, "OpGroupIAddNonUniformAMD"},
        {5001, "OpGroupFAddNonUniformAMD"},
        {5002, "OpGroupFMinNonUniformAMD"},
        {5003, "OpGroupUMinNonUniformAMD"},
        {5004, "OpGroupSMinNonUniformAMD"},
        {5005, "OpGroupFMaxNonUniformAMD"},
        {5006, "OpGroupUMaxNonUniformAMD"},
        {5007, "OpGroupSMaxNonUniformAMD"},
        {5011, "OpFragmentMaskFetchAMD"},
        {5012, "OpFragmentFetchAMD"},
        {5056, "OpReadClockKHR"},
        {5283, "OpImageSampleFootprintNV"},
        {5296, "OpGroupNonUniformPartitionNV"},
        {5299, "OpWritePackedPrimitiveIndices4x8NV"},
        {5334, "OpReportIntersectionKHR"},
        {5334, "OpReportIntersectionNV"},
        {5335, "OpIgnoreIntersectionNV"},
        {5336, "OpTerminateRayNV"},
        {5337, "OpTraceNV"},
        {5341, "OpTypeAccelerationStructureKHR"},
        {5341, "OpTypeAccelerationStructureNV"},
        {5344, "OpExecuteCallableNV"},
        {5358, "OpTypeCooperativeMatrixNV"},
        {5359, "OpCooperativeMatrixLoadNV"},
        {5360, "OpCooperativeMatrixStoreNV"},
        {5361, "OpCooperativeMatrixMulAddNV"},
        {5362, "OpCooperativeMatrixLengthNV"},
        {5364, "OpBeginInvocationInterlockEXT"},
        {5365, "OpEndInvocationInterlockEXT"},
        {5380, "OpDemoteToHelperInvocationEXT"},
        {5381, "OpIsHelperInvocationEXT"},
        {5571, "OpSubgroupShuffleINTEL"},
        {5572, "OpSubgroupShuffleDownINTEL"},
        {5573, "OpSubgroupShuffleUpINTEL"},
        {5574, "OpSubgroupShuffleXorINTEL"},
        {5575, "OpSubgroupBlockReadINTEL"},
        {5576, "OpSubgroupBlockWriteINTEL"},
        {5577, "OpSubgroupImageBlockReadINTEL"},
        {5578, "OpSubgroupImageBlockWriteINTEL"},
        {5580, "OpSubgroupImageMediaBlockReadINTEL"},
        {5581, "OpSubgroupImageMediaBlockWriteINTEL"},
        {5585, "OpUCountLeadingZerosINTEL"},
        {5586, "OpUCountTrailingZerosINTEL"},
        {5587, "OpAbsISubINTEL"},
        {5588, "OpAbsUSubINTEL"},
        {5589, "OpIAddSatINTEL"},
        {5590, "OpUAddSatINTEL"},
        {5591, "OpIAverageINTEL"},
        {5592, "OpUAverageINTEL"},
        {5593, "OpIAverageRoundedINTEL"},
        {5594, "OpUAverageRoundedINTEL"},
        {5595, "OpISubSatINTEL"},
        {5596, "OpUSubSatINTEL"},
        {5597, "OpIMul32x16INTEL"},
        {5598, "OpUMul32x16INTEL"},
        {5600, "OpFunctionPointerINTEL"},
        {5601, "OpFunctionPointerCallINTEL"},
        {5632, "OpDecorateString"},
        {5632, "OpDecorateStringGOOGLE"},
        {5633, "OpMemberDecorateString"},
        {5633, "OpMemberDecorateStringGOOGLE"},
        {5699, "OpVmeImageINTEL"},
        {5700, "OpTypeVmeImageINTEL"},
        {5701, "OpTypeAvcImePayloadINTEL"},
        {5702, "OpTypeAvcRefPayloadINTEL"},
        {5703, "OpTypeAvcSicPayloadINTEL"},
        {5704, "OpTypeAvcMcePayloadINTEL"},
        {5705, "OpTypeAvcMceResultINTEL"},
        {5706, "OpTypeAvcImeResultINTEL"},
        {5707, "OpTypeAvcImeResultSingleReferenceStreamoutINTEL"},
        {5708, "OpTypeAvcImeResultDualReferenceStreamoutINTEL"},
        {5709, "OpTypeAvcImeSingleReferenceStreaminINTEL"},
        {5710, "OpTypeAvcImeDualReferenceStreaminINTEL"},
        {5711, "OpTypeAvcRefResultINTEL"},
        {5712, "OpTypeAvcSicResultINTEL"},
        {5713, "OpSubgroupAvcMceGetDefaultInterBaseMultiReferencePenaltyINTEL"},
        {5714, "OpSubgroupAvcMceSetInterBaseMultiReferencePenaltyINTEL"},
        {5715, "OpSubgroupAvcMceGetDefaultInterShapePenaltyINTEL"},
        {5716, "OpSubgroupAvcMceSetInterShapePenaltyINTEL"},
        {5717, "OpSubgroupAvcMceGetDefaultInterDirectionPenaltyINTEL"},
        {5718, "OpSubgroupAvcMceSetInterDirectionPenaltyINTEL"},
        {5719, "OpSubgroupAvcMceGetDefaultIntraLumaShapePenaltyINTEL"},
        {5720, "OpSubgroupAvcMceGetDefaultInterMotionVectorCostTableINTEL"},
        {5721, "OpSubgroupAvcMceGetDefaultHighPenaltyCostTableINTEL"},
        {5722, "OpSubgroupAvcMceGetDefaultMediumPenaltyCostTableINTEL"},
        {5723, "OpSubgroupAvcMceGetDefaultLowPenaltyCostTableINTEL"},
        {5724, "OpSubgroupAvcMceSetMotionVectorCostFunctionINTEL"},
        {5725, "OpSubgroupAvcMceGetDefaultIntraLumaModePenaltyINTEL"},
        {5726, "OpSubgroupAvcMceGetDefaultNonDcLumaIntraPenaltyINTEL"},
        {5727, "OpSubgroupAvcMceGetDefaultIntraChromaModeBasePenaltyINTEL"},
        {5728, "OpSubgroupAvcMceSetAcOnlyHaarINTEL"},
        {5729, "OpSubgroupAvcMceSetSourceInterlacedFieldPolarityINTEL"},
        {5730, "OpSubgroupAvcMceSetSingleReferenceInterlacedFieldPolarityINTEL"},
        {5731, "OpSubgroupAvcMceSetDualReferenceInterlacedFieldPolaritiesINTEL"},
        {5732, "OpSubgroupAvcMceConvertToImePayloadINTEL"},
        {5733, "OpSubgroupAvcMceConvertToImeResultINTEL"},
        {5734, "OpSubgroupAvcMceConvertToRefPayloadINTEL"},
        {5735, "OpSubgroupAvcMceConvertToRefResultINTEL"},
        {5736, "OpSubgroupAvcMceConvertToSicPayloadINTEL"},
        {5737, "OpSubgroupAvcMceConvertToSicResultINTEL"},
        {5738, "OpSubgroupAvcMceGetMotionVectorsINTEL"},
        {5739, "OpSubgroupAvcMceGetInterDistortionsINTEL"},
        {5740, "OpSubgroupAvcMceGetBestInterDistortionsINTEL"},
        {5741, "OpSubgroupAvcMceGetInterMajorShapeINTEL"},
        {5742, "OpSubgroupAvcMceGetInterMinorShapeINTEL"},
        {5743, "OpSubgroupAvcMceGetInterDirectionsINTEL"},
        {5744, "OpSubgroupAvcMceGetInterMotionVectorCountINTEL"},
        {5745, "OpSubgroupAvcMceGetInterReferenceIdsINTEL"},
        {5746, "OpSubgroupAvcMceGetInterReferenceInterlacedFieldPolaritiesINTEL"},
        {5747, "OpSubgroupAvcImeInitializeINTEL"},
        {5748, "OpSubgroupAvcImeSetSingleReferenceINTEL"},
        {5749, "OpSubgroupAvcImeSetDualReferenceINTEL"},
        {5750, "OpSubgroupAvcImeRefWindowSizeINTEL"},
        {5751, "OpSubgroupAvcImeAdjustRefOffsetINTEL"},
        {5752, "OpSubgroupAvcImeConvertToMcePayloadINTEL"},
        {5753, "OpSubgroupAvcImeSetMaxMotionVectorCountINTEL"},
        {5754, "OpSubgroupAvcImeSetUnidirectionalMixDisableINTEL"},
        {5755, "OpSubgroupAvcImeSetEarlySearchTerminationThresholdINTEL"},
        {5756, "OpSubgroupAvcImeSetWeightedSadINTEL"},
        {5757, "OpSubgroupAvcImeEvaluateWithSingleReferenceINTEL"},
        {5758, "OpSubgroupAvcImeEvaluateWithDualReferenceINTEL"},
        {5759, "OpSubgroupAvcImeEvaluateWithSingleReferenceStreaminINTEL"},
        {5760, "OpSubgroupAvcImeEvaluateWithDualReferenceStreaminINTEL"},
        {5761, "OpSubgroupAvcImeEvaluateWithSingleReferenceStreamoutINTEL"},
        {5762, "OpSubgroupAvcImeEvaluateWithDualReferenceStreamoutINTEL"},
        {5763, "OpSubgroupAvcImeEvaluateWithSingleReferenceStreaminoutINTEL"},
        {5764, "OpSubgroupAvcImeEvaluateWithDualReferenceStreaminoutINTEL"},
        {5765, "OpSubgroupAvcImeConvertToMceResultINTEL"},
        {5766, "OpSubgroupAvcImeGetSingleReferenceStreaminINTEL"},
        {5767, "OpSubgroupAvcImeGetDualReferenceStreaminINTEL"},
        {5768, "OpSubgroupAvcImeStripSingleReferenceStreamoutINTEL"},
        {5769, "OpSubgroupAvcImeStripDualReferenceStreamoutINTEL"},
        {5770, "OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeMotionVectorsINTEL"},
        {5771, "OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeDistortionsINTEL"},
        {5772, "OpSubgroupAvcImeGetStreamoutSingleReferenceMajorShapeReferenceIdsINTEL"},
        {5773, "OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeMotionVectorsINTEL"},
        {5774, "OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeDistortionsINTEL"},
        {5775, "OpSubgroupAvcImeGetStreamoutDualReferenceMajorShapeReferenceIdsINTEL"},
        {5776, "OpSubgroupAvcImeGetBorderReachedINTEL"},
        {5777, "OpSubgroupAvcImeGetTruncatedSearchIndicationINTEL"},
        {5778, "OpSubgroupAvcImeGetUnidirectionalEarlySearchTerminationINTEL"},
        {5779, "OpSubgroupAvcImeGetWeightingPatternMinimumMotionVectorINTEL"},
        {5780, "OpSubgroupAvcImeGetWeightingPatternMinimumDistortionINTEL"},
        {5781, "OpSubgroupAvcFmeInitializeINTEL"},
        {5782, "OpSubgroupAvcBmeInitializeINTEL"},
        {5783, "OpSubgroupAvcRefConvertToMcePayloadINTEL"},
        {5784, "OpSubgroupAvcRefSetBidirectionalMixDisableINTEL"},
        {5785, "OpSubgroupAvcRefSetBilinearFilterEnableINTEL"},
        {5786, "OpSubgroupAvcRefEvaluateWithSingleReferenceINTEL"},
        {5787, "OpSubgroupAvcRefEvaluateWithDualReferenceINTEL"},
        {5788, "OpSubgroupAvcRefEvaluateWithMultiReferenceINTEL"},
        {5789, "OpSubgroupAvcRefEvaluateWithMultiReferenceInterlacedINTEL"},
        {5790, "OpSubgroupAvcRefConvertToMceResultINTEL"},
        {5791, "OpSubgroupAvcSicInitializeINTEL"},
        {5792, "OpSubgroupAvcSicConfigureSkcINTEL"},
        {5793, "OpSubgroupAvcSicConfigureIpeLumaINTEL"},
        {5794, "OpSubgroupAvcSicConfigureIpeLumaChromaINTEL"},
        {5795, "OpSubgroupAvcSicGetMotionVectorMaskINTEL"},
        {5796, "OpSubgroupAvcSicConvertToMcePayloadINTEL"},
        {5797, "OpSubgroupAvcSicSetIntraLumaShapePenaltyINTEL"},
        {5798, "OpSubgroupAvcSicSetIntraLumaModeCostFunctionINTEL"},
        {5799, "OpSubgroupAvcSicSetIntraChromaModeCostFunctionINTEL"},
        {5800, "OpSubgroupAvcSicSetBilinearFilterEnableINTEL"},
        {5801, "OpSubgroupAvcSicSetSkcForwardTransformEnableINTEL"},
        {5802, "OpSubgroupAvcSicSetBlockBasedRawSkipSadINTEL"},
        {5803, "OpSubgroupAvcSicEvaluateIpeINTEL"},
        {5804, "OpSubgroupAvcSicEvaluateWithSingleReferenceINTEL"},
        {5805, "OpSubgroupAvcSicEvaluateWithDualReferenceINTEL"},
        {5806, "OpSubgroupAvcSicEvaluateWithMultiReferenceINTEL"},
        {5807, "OpSubgroupAvcSicEvaluateWithMultiReferenceInterlacedINTEL"},
        {5808, "OpSubgroupAvcSicConvertToMceResultINTEL"},
        {5809, "OpSubgroupAvcSicGetIpeLumaShapeINTEL"},
        {5810, "OpSubgroupAvcSicGetBestIpeLumaDistortionINTEL"},
        {5811, "OpSubgroupAvcSicGetBestIpeChromaDistortionINTEL"},
        {5812, "OpSubgroupAvcSicGetPackedIpeLumaModesINTEL"},
        {5813, "OpSubgroupAvcSicGetIpeChromaModeINTEL"},
        {5814, "OpSubgroupAvcSicGetPackedSkcLumaCountThresholdINTEL"},
        {5815, "OpSubgroupAvcSicGetPackedSkcLumaSumThresholdINTEL"},
        {5816, "OpSubgroupAvcSicGetInterRawSadsINTEL"},
        {5887, "OpLoopControlINTEL"},
        {5946, "OpReadPipeBlockingINTEL"},
        {5947, "OpWritePipeBlockingINTEL"},
        {5949, "OpFPGARegINTEL"},
        {6016, "OpRayQueryGetRayTMinKHR"},
        {6017, "OpRayQueryGetRayFlagsKHR"},
        {6018, "OpRayQueryGetIntersectionTKHR"},
        {6019, "OpRayQueryGetIntersectionInstanceCustomIndexKHR"},
        {6020, "OpRayQueryGetIntersectionInstanceIdKHR"},
        {6021, "OpRayQueryGetIntersectionInstanceShaderBindingTableRecordOffsetKHR"},
        {6022, "OpRayQueryGetIntersectionGeometryIndexKHR"},
        {6023, "OpRayQueryGetIntersectionPrimitiveIndexKHR"},
        {6024, "OpRayQueryGetIntersectionBarycentricsKHR"},
        {6025, "OpRayQueryGetIntersectionFrontFaceKHR"},
        {6026, "OpRayQueryGetIntersectionCandidateAABBOpaqueKHR"},
        {6027, "OpRayQueryGetIntersectionObjectRayDirectionKHR"},
        {6028, "OpRayQueryGetIntersectionObjectRayOriginKHR"},
        {6029, "OpRayQueryGetWorldRayDirectionKHR"},
        {6030, "OpRayQueryGetWorldRayOriginKHR"},
        {6031, "OpRayQueryGetIntersectionObjectToWorldKHR"},
        {6032, "OpRayQueryGetIntersectionWorldToObjectKHR"},
        {6035, "OpAtomicFAddEXT"},
    };

    return out << op_names[op];
}
