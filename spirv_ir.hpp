#pragma once
#include "spirv_ops.hpp"

namespace spir {
// core elements
struct Type;
struct Expr;
struct Var;
struct Block;
struct Func;

struct Type {
    Id id;
};
struct Expr {
    Id id;
};
struct Var {
    Id id;
    std::shared_ptr<Type> type;
    std::weak_ptr<Block> parent_block;
};
struct Block {
    struct Termination {
        struct Return {};
        using UnconditionalJump = std::shared_ptr<Block>;
        struct ConditionalBranch {
            std::shared_ptr<Expr> condition;
            std::shared_ptr<Block> true_branch;
            std::shared_ptr<Block> false_branch;
        };
    };

    Id id;
    std::optional<std::weak_ptr<Block>> parent;
    std::map<Id, std::shared_ptr<Var>> variables;
    std::vector<std::shared_ptr<Expr>> instructions;
    std::variant<
        Termination::Return
        , Termination::UnconditionalJump
        , Termination::ConditionalBranch> termination;
};
struct Func {
    Id id;
    std::shared_ptr<Type> return_type;
    std::vector<std::shared_ptr<Type>> parameter_types;
    std::shared_ptr<Block> entry_point;
};

// types
struct Void : Type {
};
struct Bool : Type  {
};
struct Float : Type  {
    std::size_t 
        width;
};
struct Int : Type  {
    std::size_t
        width;
    bool
        signedness;
};
struct Pointer : Type  {
    std::shared_ptr<Type>
        type;
    spv::StorageClass
        storage_class;
};
struct Vector : Type {
    std::shared_ptr<Type>
        component_type;
    std::size_t
        component_count;
};
struct Struct : Type {
    std::vector<std::shared_ptr<Type>>
        member_types;
};

// exprs
struct CompositeConstruct : Expr  {
    std::shared_ptr<Type>
        result_type;
    std::vector<std::shared_ptr<Expr>>
        constituents;
};
struct CompositeExtract : Expr {
    std::shared_ptr<Type>
        result_type;
    std::shared_ptr<Expr>
        composite;
    std::vector<std::size_t>
        indices;
};
struct Constant : Expr  {
    std::shared_ptr<Type>
        result_type;
    std::aligned_storage<4>
        value;
};
struct ConstantComposite : Expr {
    std::shared_ptr<Type>
        result_type;
    std::vector<std::shared_ptr<Constant>>
        constituents;
};
struct ConvertFToS : Expr  {
    std::shared_ptr<Int>
        result_type;
    std::shared_ptr<Expr>
        float_value;
};
struct ConvertSToF : Expr {
    std::shared_ptr<Float>
        result_type;
    std::shared_ptr<Expr>
        signed_value;
};
using BinaryOperands = std::array<std::shared_ptr<Expr>, 2>;

struct FAdd : Expr {
    std::shared_ptr<Float>
        result_type;
    BinaryOperands
        operand;
};
struct FDiv : Expr {
    std::shared_ptr<Float>
        result_type;
    BinaryOperands
        operand;
};
struct FOrdGreaterThan : Expr {
    std::shared_ptr<Bool>
        result_type;
    BinaryOperands
        operand;
};
struct FSub : Expr {
    std::shared_ptr<Float>
        result_type;
    BinaryOperands
        operand;
};
struct IEqual : Expr {
    std::shared_ptr<Bool>
        result_type;
    BinaryOperands
        operand;
};
struct IMul : Expr {
    std::shared_ptr<Int>
        result_type;
    BinaryOperands
        operand;
};
struct ISub : Expr {
    std::shared_ptr<Int>
        result_type;
    BinaryOperands
        operand;
};

struct PointerExpr : Expr {};
struct AccessChain : PointerExpr {
    std::shared_ptr<Pointer>
        type;
    std::shared_ptr<Expr>
        base;
    std::vector<std::shared_ptr<Constant>>
        indices;
};
struct Phi : Var {
    std::vector<std::pair<std::weak_ptr<Var>, std::weak_ptr<Block>>> 
        operands;
};
struct Load : Expr {
    std::shared_ptr<Type>
        type;
    std::shared_ptr<PointerExpr>
        pointer;
};
struct Store : Expr {
    std::shared_ptr<PointerExpr>
        pointer;
    std::shared_ptr<Expr>
        object;
};

struct ExtInstruction  {
    GLSLstd450 instruction;
    std::shared_ptr<Type> result_type;
    std::vector<std::shared_ptr<Expr>> operand;
};

// compilation
namespace detail {
template<class X, class=void>
struct has_result_id : std::false_type {};
template<class X>
struct has_result_id<X, std::conditional_t<true, void, decltype(std::declval<X>().result)>> : std::true_type {};

struct result_id_fn {
    template<class Op>
    auto operator()(const Op& op) const -> std::optional<Id> {
        if constexpr(has_result_id<Op>::value) {
            return {op.result};
        }
        else {
            return {};
        }
    }

    auto operator()(const Instruction& op) const -> std::optional<Id> {
        return std::visit(*this, op);
    }
};
const auto result_id = result_id_fn{};
}
struct Source : std::vector<Instruction> {
    Source(const std::vector<RawInstruction>& raw) {
        for(auto& pre : raw) {
            if(auto post = spir::to_structured_instruction(pre);
                    post) {
                this->emplace_back(*post);
            }
        }
    }

    auto line(std::size_t i) const -> const Instruction& {
        return this->at(i);
    }

    auto index_of(Id id) const -> std::size_t {
        size_t i = 0;
        for(auto& line : *this) {
            if(id == detail::result_id(line)) {
                return i;
            }
            ++i;
        }
        throw std::runtime_error("invalid id");
    }

    auto instruction(Id id) const -> const Instruction& {
        return line(index_of(id));
    }
};
struct Compilation {
    struct Context {
        std::map<Id, std::shared_ptr<Type>> types;
        std::map<Id, std::shared_ptr<Expr>> exprs;
        std::map<Id, std::shared_ptr<Var>> vars;
        std::map<Id, std::shared_ptr<Func>> funcs;
        std::map<Id, std::shared_ptr<Block>> blocks;
        std::map<Id, std::string> names;
    };
    struct Scope : std::vector<std::shared_ptr<Block>> {
        auto push(std::shared_ptr<Block> block) {
            this->push_back(block);
        }
        auto pop(std::shared_ptr<Block> block) {
            this->pop_back();
        }
        auto top() {
            return this->back();
        }
    };

    const Source source;
    Context context;
    Scope scope;
    std::shared_ptr<Func> result;

    Compilation(const Source& source)
    : source(source) {
        auto op = std::get<OpEntryPoint>(source.line(0));
        result = std::make_shared<Func>();
        build(result, source.index_of(op.entry_point));
    }

private:

    void build(std::shared_ptr<Func>& ptr, std::size_t i) {
        auto op = std::get<OpFunction>(source.line(i));
        auto signature = std::get<OpTypeFunction>(source.instruction(op.function_type));

        ptr->id = op.result;
        ptr->return_type = context.types.find(op.result_type)->second;

        for(auto id : signature.parameters) {
            ptr->parameter_types.emplace_back(context.types.find(id)->second);
        }

        ptr->entry_point = std::make_shared<Block>();
        build(ptr->entry_point, i+1);

        context.funcs.emplace(ptr->id, ptr);
    }
    void build(std::shared_ptr<Block>& ptr, std::size_t i) {
        auto op = std::get<OpLabel>(source.line(i));
        ptr->id = op.result;
    }
    void build(std::shared_ptr<Void>& ptr, std::size_t i) {
        auto op = std::get<OpTypeVoid>(source.at(i));
        ptr->id = op.result;
    }
    void build(std::shared_ptr<Bool>& ptr, std::size_t i) {
        auto op = std::get<OpTypeBool>(source.at(i));
        ptr->id = op.result;
    }
    void build(std::shared_ptr<Float>& ptr, std::size_t i) {
        auto op = std::get<OpTypeFloat>(source.at(i));
        ptr->id = op.result;
        ptr->width = op.width;
    }
    void build(std::shared_ptr<Int>& ptr, std::size_t i) {
        auto op = std::get<OpTypeInt>(source.line(i));
        ptr->id = op.result;
        ptr->width = op.width;
        ptr->signedness = op.signedness;
    }
    void build(std::shared_ptr<Pointer>& ptr, std::size_t i) {
        auto op = std::get<OpTypePointer>(source.at(i));
        ptr->id = op.result;
        ptr->type = context.types.find(op.type)->second;
        ptr->storage_class = op.storage_class;
    }
    void build(std::shared_ptr<Vector>& ptr, std::size_t i) {
        auto op = std::get<OpTypeVector>(source.at(i));
        ptr->id = op.result;
        ptr->component_type = context.types.find(op.component_type)->second;
        ptr->component_count = op.component_count;
    }
    void build(std::shared_ptr<Struct>& ptr, std::size_t i) {
        auto op = std::get<OpTypeStruct>(source.at(i));
        ptr->id = op.result;
        for(auto& type : op.member_types) {
            ptr->member_types.emplace_back(context.types.find(type)->second);
        }
    }
    auto build(std::shared_ptr<ISub>& ptr, std::size_t i) {
        build_binary_expr<ISub, OpISub, Int>(ptr, i);
    }
    auto build(std::shared_ptr<FOrdGreaterThan>& ptr, std::size_t i) {
        build_binary_expr<FOrdGreaterThan, OpFOrdGreaterThan, Bool>(ptr, i);
    }
    auto build(std::shared_ptr<FDiv>& ptr, std::size_t i) {
        build_binary_expr<FDiv, OpFDiv, Float>(ptr, i);
    }
    auto build(std::shared_ptr<FAdd>& ptr, std::size_t i) {
        build_binary_expr<FAdd, OpFAdd, Float>(ptr, i);
    }
    auto build(std::shared_ptr<ConvertSToF>& ptr, std::size_t i) {
        auto op = std::get<OpConvertSToF>(source.at(i));
        ptr->id = op.result;
        ptr->result_type = std::static_pointer_cast<Float>(context.types.find(op.result_type)->second);
        ptr->signed_value = context.exprs.find(op.signed_value)->second;
    }
    auto build(std::shared_ptr<ConvertFToS>& ptr, std::size_t i) {
        auto op = std::get<OpConvertFToS>(source.at(i));
        ptr->id = op.result;
        ptr->result_type = std::static_pointer_cast<Int>(context.types.find(op.result_type)->second);
        ptr->float_value = context.exprs.find(op.float_value)->second;
    }
    auto build(std::shared_ptr<ConstantComposite>& ptr, std::size_t i) {
        auto op = std::get<OpConstantComposite>(source.at(i));
        ptr->id = op.result;
        ptr->result_type = context.types.find(op.result_type)->second;
        for(auto& id : op.constituents) {
            auto c = std::static_pointer_cast<Constant>(context.exprs.find(id)->second);
            if(not c) {
                throw std::runtime_error("expected Constant");
            }
            ptr->constituents.emplace_back(std::move(c));
        }
    }
    auto build(std::shared_ptr<CompositeConstruct>& ptr, std::size_t i) {
        auto op = std::get<OpCompositeConstruct>(source.at(i));
        ptr->id = op.result;
        ptr->result_type = context.types.find(op.result_type)->second;
        for(auto& expr : op.constituents) {
            ptr->constituents.emplace_back(context.exprs.find(expr)->second);
        }
    }
    auto build(std::shared_ptr<CompositeExtract>& ptr, std::size_t i) {
        auto op = std::get<OpCompositeExtract>(source.at(i));
        ptr->id = op.result;
        ptr->result_type = context.types.find(op.result_type)->second;
        ptr->composite = context.exprs.find(op.composite)->second;
        ptr->indices = op.indices;
    }
    auto build(std::shared_ptr<Constant>& ptr, std::size_t i) {
        auto op = std::get<OpConstant>(source.at(i));
        ptr->id = op.result;
        ptr->result_type = context.types.find(op.result_type)->second;
        ptr->value = op.value;
    }
    auto build(std::shared_ptr<FSub>& ptr, std::size_t i) {
        build_binary_expr<FSub, OpFSub, Float>(ptr, i);
    }
    auto build(std::shared_ptr<IEqual>& ptr, std::size_t i) {
        build_binary_expr<IEqual, OpIEqual, Bool>(ptr, i);
    }
    auto build(std::shared_ptr<IMul>& ptr, std::size_t i) {
        build_binary_expr<IMul, OpIMul, Int>(ptr, i);
    }
    auto build(std::shared_ptr<Store>& ptr, std::size_t i) {
        auto op = std::get<OpStore>(source.at(i));
        ptr->id = 0;
        ptr->pointer = std::static_pointer_cast<PointerExpr>(context.exprs.find(op.pointer)->second);
        ptr->object = context.exprs.find(op.object)->second;
    }
    auto build(std::shared_ptr<AccessChain>& ptr, std::size_t i) {
        auto op = std::get<OpAccessChain>(source.at(i));
        ptr->id = op.result;
        ptr->type = std::static_pointer_cast<Pointer>(context.types.find(op.result_type)->second);
        ptr->base = context.exprs.find(op.result_type)->second;
        for(auto id : op.indices) {
            ptr->indices.emplace_back(std::static_pointer_cast<Constant>(context.exprs.find(id)->second));
        }
    }
    auto build(std::shared_ptr<Phi>& ptr, std::size_t i) {
        auto op = std::get<OpPhi>(source.at(i));
        ptr->id = op.result;
        ptr->type = context.types.find(op.result_type)->second;
        for(auto[ var, parent ] : op.variable_parent) {
            ptr->operands.emplace_back(
                context.vars.find(var)->second,
                context.blocks.find(parent)->second
            );
        }
    }
    auto build(std::shared_ptr<Load>& ptr, std::size_t i) {
        auto op = std::get<OpLoad>(source.at(i));
        ptr->id = op.result;
        ptr->type = context.types.find(op.result_type)->second;
        ptr->pointer = std::static_pointer_cast<PointerExpr>(context.exprs.find(op.pointer)->second);
    }
    auto build(std::shared_ptr<ExtInstruction>& ptr, std::size_t i) {
        auto op = std::get<OpExtInst>(source.at(i));
        ptr->instruction = static_cast<GLSLstd450>(op.instruction);
        ptr->result_type = context.types.find(op.result_type)->second;
        for(auto operand : op.operand) {
            ptr->operand.emplace_back(context.exprs.find(operand)->second);
        }
    }

private:

    template<class Expr, class Op, class Result>
    void build_binary_expr(std::shared_ptr<Expr>& ptr, std::size_t i) {
        auto op = std::get<Op>(source.at(i));
        ptr->id = op.result;
        ptr->result_type = std::static_pointer_cast<Result>(context.types.find(op.result_type)->second);
        for(auto i = 0; i < 2; ++i) {
            ptr->operand[i] = context.exprs.find(op.operand[i])->second;
        }
    }

};
inline
auto compile(const Source& from_source) -> std::shared_ptr<Func> {
    return Compilation{from_source}.result;
}
}
