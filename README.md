# spirv2hvx
spir-v to halide transpiler

should actually be called spirv2halide but its fine for now

## how to use
standard cmake-make build process

invocation:
`./spirv2hvx shader.spv`

where `shader.spv` is a raw spir-v file (not human readable)

this will produce an "almost-ready" halide source code corresponding to the spir-v.
it gets the broad strokes right, but some manual touchup is required.

## future work
the files `spirv_halide.*` contain the existing halide transpiler.

it is based on (and inherits from) the spirv-cross glsl transpiler.

this was an ad-hoc solution as the glsl transpiler is not that well suited to targeting halide.

in the original approach, spir-v bytecode was read into a `CompilerGLSL`,
which would traverse the instructions and build up centralized state describing the program
and provide id-based getter/setter methods for modifying this state in-place.

finally, the instructions are traversed in a second pass and this is used, 
along with querying the compiler state,
to emit target code.

instead, a more flexible approach using a typed AST is implemented in `spirv_ir.hpp`.

the transpilation process is further subdivided into self-contained, composable passes
(as opposed to stateful in-place modification which makes no compile-time distinction between different phases of compilation).

first, the raw bytecode stream is converted into "mostly-typed" instructions which use untyped id numbers as references.

next, this flattened representation is converted into an abstract syntax tree, 
with id number references completely replaced with pointers to the corresponding node in the tree
(either by looking up previously-compiled results in the `Context` cache
or by recursively compiling a forward-referenced id by looking ahead in the instructions).

`Scope` is a stack of `Blocks` used to track the parent blocks of variables, branches and merges.

this tree can then be traversed to produce halide source code.

### additional details for future work
`Vars` will need some way to track `OpStore` instructions targeting them or 
(in the case of composite types) their components.
this is because vectors are implemented as `Halide::Func` 
and attempting to redefine them is an error in some cases.

so `Vars` will need to generate alias `Exprs` corresponding to "variable after store operation x",
and `OpLoads` targeting that `Var` in the future will need to refer to the alias `Expr`
corresponding to the last store operation targeting that `Var` in the instruction stream.
