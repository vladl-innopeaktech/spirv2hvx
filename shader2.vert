#version 320 es
layout (location = 0) in vec3 gPos;
layout (location = 1) in vec2 gTexCoords;
layout (location = 2) out vec2 TexCoords;
void main()
{
    highp vec3 v0 = vec3(1, 1.2, 0.5);
    highp vec3 v1 = vec3(1, 0.2, 0.5);
    highp vec3 v2;
    v2 = v0 * gPos;
    v2 = v1 * v2;
    v1 = v2 * gPos;
    v2 = pow(gPos, v1);
    v2 = v2 + gPos;
    TexCoords = gTexCoords;
    gl_Position = vec4(v2, 1.0f);
}
